import java.util.*;
import java.net.*;
import java.io.*;

public class TicTacToe {
  static final int DEFAULT_PORT = 4176;
  static final String ERR_OK = "0 OK";
  static final String ERR_SYNTAX = "100 ERR syntax error";
  static final String ERR_BAD_VERSION = "101 ERR Unknown Version Number";

  static boolean debug = false;

  public static void main( String [] args ) {
    try {
      int port = DEFAULT_PORT;

      for( String a : args ) {
        if( a.equals( "-d" ) ) {
          debug = true;
        } else {
          try {
            port = Integer.parseInt( a );
          } catch ( Exception e ) {
            System.out.println( "Unexpected argument: " + a );
            System.out.println( "Usage: java QMeServer [port #] [-d]" );
          }
        }
      }

      System.out.println( "Setting Up" );
      ServerSocket ss = new ServerSocket( port );

      System.out.println( "Entering Main Loop" );
      while( true ) {
        Thread t = new Thread( new ConnectionThread( ss.accept() ) );
        t.start();
      }
    } catch ( Exception e ) {
      if( debug ) {
        System.out.println( e );
      }
    }
  }

  public static class ConnectionThread implements Runnable {
    private Socket sock;
    public final String VER = "TicTacToe/1.0";
    public final char DOT = '.';
    public final char EX = 'x';
    public final char OH = 'o';
    private PrintStream out;
    private Scanner in;
    private char [] board = new char[9];
    private int move = -1;
    private int turns = 0;
    private char player = DOT;
    private char server = DOT;

    ConnectionThread( Socket s ) throws Exception {
      sock = s;
      in = new Scanner( sock.getInputStream() );
      out = new PrintStream( sock.getOutputStream(), true );
    }

    private String doStart( Scanner s ) {
      String err = ERR_SYNTAX;

      if( ( player == DOT ) && s.hasNext() ) {
        String start = s.next();

        if( start != null ) {
          if( start.equals( "x" ) || start.equals( "X" ) ) {
            player = EX;
            server = OH;
            err = ERR_OK;
          } else if( start.equals( "o" ) || start.equals( "O" ) ) {
            player = OH;
            server = EX;
            move = 0;
            board[move] = server;
            turns++;
            err = ERR_OK;
          }
        }
      }
      return err;
    }

    private static final int [][] row = { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 },
                                          { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 },
                                          { 0, 4, 8 }, { 2, 4, 6 } };
               
    private boolean isWin( char p ) {
      for( int i = 0; i < row.length; i++ ) {
        boolean win = true;
        for( int j = 0; j < row[i].length; j++ ) {
          win &= board[row[i][j]] == p;
        }
        if( win ) {
          return true;
        }
      }
      return false;
    }

    private int evaluate( char us, char them ) {
      if( isWin( us ) ) {
        return 1;
      }

      int worst = -2;
      for( int i = 0; i < 9; i++ ) {
        if( board[i] == DOT ) {
          board[i] = them;
          int result = evaluate( them, us );
          if( result > worst ) {
            worst = result;
          }
          board[i] = DOT;
        }
      }
      if( worst == -2 ) {
        return 0;
      }
      return -worst;
    }   
          
    private String doMove( Scanner s ) {
      String err = ERR_SYNTAX;

      if( ( player != DOT ) && s.hasNextInt() ) {
        int next = s.nextInt();

        if( ( next >= 0 ) && ( next < 9 ) && ( board[next] == DOT ) ) {
          board[next] = player;
          move = -1;
          turns++;
          if( turns < board.length ) {
            int best = -1;
            for( int i = 0; i < 9; i++ ) {
              if( board[i] == DOT ) {
                board[i] = server;
                int e = evaluate( server, player );
                if( ( e > best ) || ( move == -1 ) ) {
                  move = i;
                  best = e;
                }
                board[i] = DOT;
              }
            }
            board[move] = server;
            turns++;
          }
          err = ERR_OK;
        }
      }
      return err;
    }

    public void run() {
      String err = ERR_SYNTAX;
      StringBuffer rep = new StringBuffer();

      for( int i = 0; i < 9; i++ ) {
        board[i] = DOT;
      }

      try {
        boolean done = false;
        while( !done ) { 
          Scanner s = new Scanner( in.nextLine() );
          try {
            String ver = s.next();
            String req = s.next();
 
            if( !ver.equals( VER ) ) {
              err = ERR_BAD_VERSION;
            } else if( req.equals( "start" ) ) {
              err = doStart( s );
            } else if( req.equals( "move" ) ) {
              err = doMove( s );
            } else if( req.equals( "quit" ) ) {
              break;
            } else {
              err = ERR_SYNTAX;
            }
          } catch ( Exception e ) {
              System.out.println( e);
              e.printStackTrace();
            err = ERR_SYNTAX;
          }

          if( out != null ) {
            out.println(err); System.out.println(err);
          }

          if( err == ERR_OK ) {
            out.println(board[0] + " " + board[1] + " " + board[2]); System.out.println(board[0] + " " + board[1] + " " + board[2]);
            out.println(board[3] + " " + board[4] + " " + board[5]); System.out.println(board[3] + " " + board[4] + " " + board[5]);
            out.println(board[6] + " " + board[7] + " " + board[8]); System.out.println(board[6] + " " + board[7] + " " + board[8]);

            done = true;
            if( isWin( player ) ) {
              out.println("done you win"); System.out.println("done you win");
            } else if( isWin( server ) ) {
              out.println("done you lose"); System.out.println("done you lose");
            } else if( turns >= board.length ) {
              out.println("done we tie"); System.out.println("done we tie");
            } else {
              if( move >= 0 ) {
                out.println("move " + move); System.out.println("move " + move);
              } else {
                out.println("your move"); System.out.println("your move");
              }
              done = false;
            }
          }
        }
        sock.close();
      } catch( Exception e ) {
        try {
          if( sock != null ) {
            sock.close();
          }
        } catch ( Exception e2 ) { }
      }
    }
  }
}
