//
//  Connection.m
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import "Connection.h"

#define kReadBufferSize 512
#define kWriteBufferInitialSize 512

@interface Connection ()
    @property(strong, nonatomic) NSMutableString * incompleteMessage;
    @property(strong, nonatomic) NSInputStream * input;
    @property(strong, nonatomic) NSOutputStream * output;
    @property(strong, nonatomic) NSMutableData * readBuffer;
    @property(strong, nonatomic) NSMutableData * writeBuffer;
    @property(assign, nonatomic) BOOL inputOpened;
    @property(assign, nonatomic) BOOL outputOpened;
    @property(assign, nonatomic) BOOL outputHasSpace;

    - (void)beginStream:(NSStream *)stream;
    - (void)endStream:(NSStream *)stream;
    - (NSString *)receiveMessage;
- (void)sendMessage:(NSString *)message;
@end;

@implementation Connection

@synthesize host = _host;
@synthesize port = _port;
@synthesize runLoop = _runLoop;
@synthesize runLoopMode = _runLoopMode;
@synthesize delegate = _delegate;

@synthesize input = _input;
@synthesize output = _output;
@synthesize readBuffer = _readBuffer;
@synthesize writeBuffer = _writeBuffer;
@synthesize inputOpened = _inputOpened;
@synthesize outputOpened = _outputOpened;
@synthesize outputHasSpace = _outputHasSpace;
@synthesize incompleteMessage = _incompleteMessage;



+ (id)connectionWithHost:(NSString *)host port:(int)port {
    return [[Connection alloc] initWithHost:host port:port];
}


// block parent default initializer (parameters are required)
- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (id)initWithHost:(NSString *)host port:(int)port {
    return [self initWithHost:host port:port runLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (id)initWithHost:(NSString *)host port:(int)port runLoop:(NSRunLoop *)runLoop forMode:(NSString *)runLoopMode {
    //verifying what we are receiving. Will spit up some errors if there's something missing
    NSAssert(host != nil, @"Host not specified");
    NSAssert1(port > 0 && port < 65536, @"Port out of range. Should be between 1 and 65535, but received %d", port);
    NSAssert(runLoop != nil, @"Run loop not specified");
    NSAssert(runLoopMode != nil, @"Run loop mode not specified");
    
    self = [super init];
    if (self) {
        _host = [host copy];
        _port = port;
        _runLoop = runLoop;
        _runLoopMode = runLoopMode;
        _delegate = nil;
        _incompleteMessage = nil;
        _input = nil;
        _output = nil;
        _readBuffer = nil;
        _writeBuffer = nil;
        _inputOpened = false;
        _outputOpened = false;
        _outputHasSpace = false;
    }
    return self;
}


- (void)beginStream:(NSStream *)stream {
    stream.delegate = self;
    [stream scheduleInRunLoop:self.runLoop forMode:self.runLoopMode];
    [stream open];
}

- (void)endStream:(NSStream *)stream {
    [stream close];
    [stream removeFromRunLoop:self.runLoop forMode:self.runLoopMode];
}

- (void) open {
    NSLog(@"Going to try %@:%i", self.host, self.port);
    //if the streams are already opened, nothing to do here
    if (self.input || self.output) {
        NSLog(@"Tried to open again the connection %@.", self);
        return;
    }
    self.readBuffer = [NSMutableData dataWithLength:kReadBufferSize];
    self.writeBuffer = [[NSMutableData alloc] initWithCapacity:kWriteBufferInitialSize];
    
    //creating the streams
    CFReadStreamRef read = NULL;
    CFWriteStreamRef write = NULL;
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)self.host, self.port, &read, &write);
    self.input = (__bridge_transfer NSInputStream *)read;
    self.output = (__bridge_transfer NSOutputStream *)write;
    [self beginStream:self.input];
    [self beginStream:self.output];
}

- (void) close {
    [self endStream:self.input];
    [self endStream:self.output];
    self.readBuffer = nil;
    self.writeBuffer = nil;
    
    //restoring initial state, so the object can be reused
    self.input = nil;
    self.output = nil;
    self.inputOpened = false;
    self.outputOpened = false;
    self.outputHasSpace = false;
}

#pragma mark -
#pragma mark Server communication

- (NSString *)receiveMessage {
    void * buffer = [self.readBuffer mutableBytes];
    NSInteger readLength = [self.input read:buffer maxLength:self.readBuffer.length];
    
    if (readLength < 0) readLength = 0;
    
    NSMutableString * message = [[NSMutableString alloc] initWithBytes:buffer
                                                                length:(NSUInteger)readLength
                                                              encoding:NSASCIIStringEncoding];
    bool complete = false;
    if (self.incompleteMessage == nil) {
        self.incompleteMessage = message;
        NSArray * spacedParts = [self.incompleteMessage componentsSeparatedByString:@" "];
        complete = ([[spacedParts objectAtIndex:0] integerValue] >= 100);
    }
    else
        [self.incompleteMessage appendString:message];
    
    if (!complete) {
        int numberOfLines = [[self.incompleteMessage componentsSeparatedByString:@"\n"] count] - 1;
        complete = (numberOfLines >= 5);
    }
    
    if (complete) {
        NSString * finalString = self.incompleteMessage;
        self.incompleteMessage = nil;
        return finalString;
    }
    else
        return @"";
}

- (void)sendMessage:(NSString *)message {
    NSLog(@"Sending this to server: %@", message);
    NSData * data = [message dataUsingEncoding:NSASCIIStringEncoding];
    [self.writeBuffer appendData:data];
    if (self.outputHasSpace)
        [self sendDataFromBuffer];
}

// Called whenever there is unsent data in our output buffer and the write
// stream has informed us that it has space available. Pushes as much data from
// the output buffer onto the output stream as it will allow, removing the
// send data from the buffer.
- (void)sendDataFromBuffer {
    NSMutableData* data = self.writeBuffer;
    if (data.length > 0) {
        NSInteger bytesWritten = [self.output write:data.bytes maxLength:data.length];
        if (bytesWritten < 0)
            bytesWritten = 0;
        
        // remove sent data from start of buffer
        [data replaceBytesInRange:NSMakeRange(0, (NSUInteger)bytesWritten) withBytes:NULL length:0];
        self.outputHasSpace = false;
    }
}

#pragma mark -
#pragma mark The big Switch of Stream Events

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode {
    switch (eventCode) {
        case NSStreamEventOpenCompleted: {
            if (stream == self.input && !self.inputOpened)
                self.inputOpened = true;
            else if (stream == self.output && !self.outputOpened)
                self.outputOpened = true;
            else
                break;
            
            if (self.inputOpened && self.outputOpened && [self.delegate respondsToSelector:@selector(connectionEstablished)])
                [self.delegate connectionEstablished];
        break; }
            
        case NSStreamEventHasBytesAvailable: {
            NSAssert(stream == self.input, @"NSStreamEventHasBytesAvailable is only for INPUT");
            NSString * data = [self receiveMessage];
            if (data.length > 0 && [self.delegate respondsToSelector:@selector(dataReceived:)])
                [self.delegate dataReceived:data];
        break; }
            
            
        case NSStreamEventHasSpaceAvailable: {
            NSAssert(stream == self.output, @"NSStreamEventHasSpaceAvailable is only for OUTPUT");
            self.outputHasSpace = true;
            [self sendDataFromBuffer];
        break; }
            
        case NSStreamEventErrorOccurred: {
            if ([self.delegate respondsToSelector:@selector(errorOccurred:)])
                [self.delegate errorOccurred:[stream streamError]];
        break; }
            
        case NSStreamEventEndEncountered: {
            if ([self.delegate respondsToSelector:@selector(endOfInputReached)])
                [self.delegate endOfInputReached];
        break; }
    }
}

@end
