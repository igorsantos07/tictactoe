//
//  IGSGameViewController.m
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import "IGSGameViewController.h"
#import "IGSAppDelegate.h"

@interface IGSGameViewController ()

@end

@implementation IGSGameViewController
@synthesize client;
@synthesize viewConnection;
@synthesize fieldServerIP;
@synthesize fieldServerPort;
@synthesize alertError;
@synthesize errorLabel;
@synthesize errorSegmentedButton;

@synthesize viewGame;
@synthesize indicatorBox;
@synthesize indicatorImg;
@synthesize indicatorTxt;
@synthesize alertGameOver;
@synthesize gameOverFace;
@synthesize gameOverLabel;
@synthesize gameOverSegmentedButton;
@synthesize boardButtons;

char playerChar;
NSString * AIIcon;
NSString * playerIcon;
NSString * playerText;
UIImage * AIBoardIcon;
UIImage * playerSmallIcon;
UIImage * playerBoardIcon;

NSString * lastMessage;

#define kPlayerHuman 0
#define kPlayerAI    1
#define kRetryYes 0
#define kRetryNo  1
#define kPlayAgain   0
#define kOtherServer 1
#define kStatistics  2

#pragma mark -
#pragma mark Boring iOS stuff

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Game", @"Game");
        self.tabBarItem.image = [UIImage imageNamed:@"tab-game-bw"];
    }
    return self;
}
							
- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload {
    [self setFieldServerIP:nil];
    [self setFieldServerPort:nil];
    [self setIndicatorImg:nil];
    [self setIndicatorTxt:nil];
    [self setAlertError:nil];
    [self setViewConnection:nil];
    [self setViewGame:nil];
    [self setAlertGameOver:nil];
    [self setBoardButtons:nil];
    [self setGameOverFace:nil];
    [self setGameOverLabel:nil];
    [self setErrorLabel:nil];
    [self setErrorSegmentedButton:nil];
    [self setGameOverSegmentedButton:nil];
    [self setIndicatorBox:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


#pragma mark -
#pragma mark Connection methods

- (void)connectionEstablished {
    NSLog(@"Here we go!");
    [self toGame];
    [self setupGameboard];
    [client sendStart:playerChar];
}

- (void)connectionClosed {
    NSLog(@"Gracefully closing...");
//    [self toConnection];
}

- (void)connectionKilled {
    NSLog(@"Crap, kicked :(");
    [self alertError:@"Oops...\nConnection lost."];
}

- (void)errorOccurred:(NSError *)error {
    NSLog(@"Error: %@", error);
    [self alertError:[error localizedDescription]];
}

- (void)attemptReconnection {
    [client connect];
}


#pragma mark -
#pragma mark Moving through views

- (void)toConnection {
    viewGame.hidden = true;
    viewConnection.hidden = false;
}

- (void)toGame {
    viewGame.hidden = false;
    viewConnection.hidden = true;
}

- (void)alertError:(NSString *)message {
    [self toConnection];
    alertError.hidden = false;
    errorLabel.text = message;
}


#pragma mark -
#pragma mark Game changes to/from the server

- (IBAction)playerMoved:(UIButton *)sender {
    [self cellUsedByIcon:playerBoardIcon cell:sender];
    [client sendMoveTo:sender.tag];
}

- (void)AIMoved:(int)cell {
    for (UIButton * button in boardButtons) {
        if (button.tag == cell) {
            [self cellUsedByIcon:AIBoardIcon cell:button];
            break;
        }
    }
}

- (void)showServerLastMove:(NSString *)move {
    NSString * lastBoard = [self getCleanedBoard:lastMessage],
             * thisBoard = [self getCleanedBoard:move];
    
    int changedCell = -1;
    for (int i = 0; i < lastBoard.length; i++) {
        if ([lastBoard characterAtIndex:i] != [thisBoard characterAtIndex:i]) {
            changedCell = i;
            break;
        }
    }
    
    if (changedCell >= 0)
        [self AIMoved:changedCell];
}

- (void)cellUsedByIcon:(UIImage *)icon cell:(UIButton *)cell {
    [cell setEnabled:false];
    [cell setImage:icon forState:UIControlStateDisabled];
}

- (NSString *)getCleanedBoard:(NSString *)message {
    NSMutableArray * parts = (NSMutableArray *)[message componentsSeparatedByString:@"\n"];
    [parts removeObjectAtIndex:0];
    [parts removeLastObject]; [parts removeLastObject]; //twice because we have an empty element in the end
    NSString * board = [parts componentsJoinedByString:@""];
    return [board stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)messageReceived:(NSString *)message {
    NSArray * parts = [message componentsSeparatedByString:@"\n"];
    NSArray * status = [[parts objectAtIndex:0] componentsSeparatedByString:@" "];

    //used when needing further processing in normal flow conditions
    NSArray * newParts;
    NSString * nextStep;
    switch ([[status objectAtIndex:0] integerValue]) {
        case 0:
            nextStep = [parts objectAtIndex:([parts count]-2)];
            newParts = [nextStep componentsSeparatedByString:@" "];

            if ([[newParts objectAtIndex:0] isEqualToString:@"move"]) {
                [self AIMoved:[[newParts objectAtIndex:1] integerValue]];
            }
            else if ([[newParts objectAtIndex:0] isEqualToString:@"your"]) {
                //do nothing. the user will move and the game flows normally
            }
            else if ([[newParts objectAtIndex:0] isEqualToString:@"done"]) {
                [self showServerLastMove:message];
                [self gameOver:[newParts objectAtIndex:([newParts count]-1)]];
            }
            lastMessage = message; //no point on saving the last message if it wasn't from the normal game flow
        break;
            
        case 100:
            NSLog(@"Syntax error! What you said to him, Fred??");
        break;
            
        case 101:
            NSLog(@"Unknown Version! What did you do, Fred?? '%@'", [[parts objectAtIndex:0] substringFromIndex:4]);
        break;
        
        default:
            NSLog(@"There's something SO weird happening... '%@'", [parts objectAtIndex:0]);
        break;
    }
}

- (void)gameOver:(NSString *)status {
    UIImage * face;
    NSString * text;
    int playerType = (playerChar == 'x')? PlayerClear : PlayerCloudy;
    
    if ([status isEqualToString:@"give up"]) {
        face = [UIImage imageNamed:@"32-face-tie.png"];
        text = @"You gave up...";
        [self statAddToType:StatGiveUp forPlayer:playerType];
    }
    else {
        [self statAddToType:StatPlayed forPlayer:playerType];
        
        if ([status isEqualToString:@"tie"]) {
            face = [UIImage imageNamed:@"32-face-tie.png"];
            text = @"Oops... It's a tie.";
            [self statAddToType:StatTie forPlayer:playerType];
        }
        else if ([status isEqualToString:@"win"]) {
            face = [UIImage imageNamed:@"32-face-win.png"];
            text = @"Congratz! You won!";
            [self statAddToType:StatWin forPlayer:playerType];
        }
        else if ([status isEqualToString:@"lose"]) {
            face = [UIImage imageNamed:@"32-face-lose.png"];
            text = @"Aww... You lose.";
            [self statAddToType:StatLose forPlayer:playerType];
        }
        else {
            NSLog(@"Received end-of-game status is: %@", status);
            face = [UIImage imageNamed:@"128-error.png"];
            text = @"Not sure what happened :(";
        }
    }
    
    [gameOverFace setImage:face];
    [gameOverLabel setText:text];
    [indicatorBox setHidden:true];
    [alertGameOver setHidden:false];
    [client disconnect];
}


#pragma mark -
#pragma mark General user actions

- (IBAction)userDidStartGame {
    client = [[GameClient alloc] initWithHost:fieldServerIP.text port:[fieldServerPort.text integerValue]];
    client.delegate = self;
    [client connect];
}

- (IBAction)userDidGiveUp:(id)sender {
    [self gameOver:@"give up"];
}

- (IBAction)buttonRetryPressed:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case kRetryYes:
            [self attemptReconnection];
        break;
            
        case kRetryNo:
            alertError.hidden = true;
        break;
    }
    sender.selectedSegmentIndex = -1;
}

- (IBAction)gameOverButtonsPressed:(UISegmentedControl *)sender {
    IGSAppDelegate * delegate = (IGSAppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [alertGameOver setHidden:true];
    switch (sender.selectedSegmentIndex) {
        case kPlayAgain:
            [self userDidStartGame];
        break;
           
        case kOtherServer:
            [self toConnection];
        break;
            
        case kStatistics:
            [self toConnection];
            [delegate.tabBarController setSelectedIndex:1];
        break;
    }
    
    sender.selectedSegmentIndex = -1;
}


#pragma mark -
#pragma mark Somewhat boring startup methods

- (void)setupGameboard {
    [self setPlayersData];
    NSString * iconMainName = [NSString stringWithFormat:@"%@%@", playerIcon, @".png"];
    playerSmallIcon = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", @"32-", iconMainName]];
    playerBoardIcon = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", @"64-", iconMainName]];
    AIBoardIcon     = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@%@", @"64-", AIIcon, @".png"]];
    indicatorImg.image = playerSmallIcon;
    indicatorTxt.text  = playerText;
    indicatorBox.hidden = false;
    [self clearBoard];
}

//should explain the mechanics behind the player icons
- (void)setPlayersData {
    NSString *clearIcon, *clearText, *cloudyIcon, *cloudyText;
    
    //getting the hour number...
    NSDateFormatter * outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH"];
    int hour = [[outputFormatter stringFromDate:[NSDate date]] integerValue];
    
    //...and setting the clear day variables
    if (hour >= 6 && hour <= 18) {
        clearIcon = @"clear-day";
        clearText = @"The Sun";
    }
    else {
        clearIcon = @"clear-night";
        clearText = @"The Moon";
    }
    
    //and now setting the cloudy day variables according to the settings
    int choosenCloud = [settings integerForKey:KeyCloudyIcon];
    if (choosenCloud == CloudyThunderstorm) {
        cloudyIcon = @"cloudy-thunderstorm";
        cloudyText = @"The Thunder";
    }
    else if (choosenCloud == CloudyOvercast) {
        cloudyIcon = @"cloudy-overcast";
        cloudyText = @"The Clouds";
    }
    else if (choosenCloud == CloudyShowers) {
        cloudyIcon = @"cloudy-showers";
        cloudyText = @"The Rain";
    }
    else if (choosenCloud == CloudyStorm) {
        cloudyIcon = @"cloudy-storm";
        cloudyText = @"The Storm";
    }
    else if (choosenCloud == CloudySnow) {
        cloudyIcon = @"cloudy-snow";
        cloudyText = @"The Snow";
    }
    
    //finally, setting the properties based on what user did in the settings
    int playerOption = [settings integerForKey:KeyPlayer];
    if (playerOption == PlayerClear) {
        playerChar = 'x';
        playerIcon = clearIcon;
        playerText = clearText;
        AIIcon = cloudyIcon;
    }
    else {
        playerChar = 'o';
        playerIcon = cloudyIcon;
        playerText = cloudyText;
        AIIcon = clearIcon;
    }
}

- (void)clearBoard {
    for (UIButton * button in boardButtons)
        button.enabled = true;
}

@end
