//
//  Connection.h
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConnectionDelegate <NSObject>
    @optional
        - (void)connectionEstablished;
        - (void)endOfInputReached;
        - (void)dataReceived:(NSString *)data;
        - (void)errorOccurred:(NSError *)error;
@end

@interface Connection : NSObject <NSStreamDelegate>
    @property(copy,readonly,nonatomic)      NSString  * host;
    @property(assign,readonly,nonatomic)    int         port;
    @property(weak,readonly,nonatomic)      NSRunLoop * runLoop;
    @property(copy,readonly,nonatomic)      NSString  * runLoopMode;

    @property(weak,nonatomic) id<ConnectionDelegate> delegate;

    + (id)connectionWithHost:(NSString *)host port:(int)port;

    - (id)initWithHost:(NSString *)host port:(int)port;
    - (id)initWithHost:(NSString *)host port:(int)port runLoop:(NSRunLoop *)runLoop forMode:(NSString *)runLoopMode;
    - (void) open;
    - (void) close;
    - (void) sendMessage:(NSString *)message;
@end
