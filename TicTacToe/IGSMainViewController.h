//
//  IGSMainViewController.h
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import <UIKit/UIKit.h>

extern NSUserDefaults * settings;
extern NSString * const KeyPlayer;
extern NSString * const KeyCloudyIcon;

extern int const PlayerClear;
extern int const PlayerCloudy;
extern int const CloudyOvercast;
extern int const CloudyShowers;
extern int const CloudyStorm;
extern int const CloudyThunderstorm;
extern int const CloudySnow;

extern int const StatWin;
extern int const StatTie;
extern int const StatLose;
extern int const StatGiveUp;
extern int const StatPlayed;
extern int const StatTotal;
extern int const PlayerTotals;

@interface IGSMainViewController : UIViewController
    - (void)settingsDidChange:(NSNotification *)notification;
    - (void)statAddToType:(int)type forPlayer:(int)player;
    - (int)statForType:(int)type player:(int)player;
    - (id)statistics;
@end
