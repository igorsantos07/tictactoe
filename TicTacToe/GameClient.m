//
//  GameClient.m
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import "GameClient.h"
#import "Connection.h"

NSString * const Prefix = @"TicTacToe/1.0";

@interface GameClient () <ConnectionDelegate>
    @property(strong, nonatomic) Connection * connection;
@end


@implementation GameClient

@synthesize delegate = _delegate;
@synthesize connection;


#pragma mark -
#pragma mark Initialization

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (id)initWithHost:(NSString *)host port:(int)port {
    self = [super init];
    
    if (self)
        connection = [Connection connectionWithHost:host port:port];
    
    return self;
}


#pragma mark -
#pragma mark Connection stuff

- (void)connect {
    NSLog(@"Connecting to server.");
    connection.delegate = self;
    [connection open];
}

- (void)disconnect {
    NSLog(@"Disconnected.");
    [connection close];
    if ([self.delegate respondsToSelector:@selector(connectionClosed)])
        [self.delegate connectionClosed];
}

- (void)endOfInputReached {
    NSLog(@"Server died!");
    [connection close];
    if ([self.delegate respondsToSelector:@selector(connectionKilled)])
        [self.delegate connectionKilled];
}

- (void)connectionEstablished {
    NSLog(@"Connected.");
    if ([self.delegate respondsToSelector:@selector(connectionEstablished)])
        [self.delegate connectionEstablished];
}

- (void)errorOccurred:(NSError *)error {
    NSLog(@"Network error: %@", [error localizedDescription]);
    [connection close];
    if ([self.delegate respondsToSelector:@selector(errorOccurred:)])
        [self.delegate errorOccurred:error];
}


#pragma mark -
#pragma mark Basic communication methods

- (void)dataReceived:(NSString *)message {
    NSLog(@"Server said something:\n%@", message);
    [self.delegate messageReceived:message];
}

- (void)sendMessage:(NSString *)message {
    [connection sendMessage:[NSString stringWithFormat:@"%@ %@%c", Prefix, message, '\n']];
}


#pragma mark -
#pragma mark Methods for quick communication with server

- (void)sendStart:(char)player {    [self sendMessage:[NSString stringWithFormat:@"start %c", player]]; }
- (void)sendMoveTo:(int)cell {      [self sendMessage:[NSString stringWithFormat:@"move %i", cell]];    }
- (void)sendQuit {                  [self sendMessage:@"quit"]; }

@end
