//
//  IGSAppDelegate.h
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGSAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

- (NSMutableArray *)statistics;

@end
