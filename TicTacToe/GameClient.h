//
//  GameClient.h
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameClientDelegate <NSObject>
    @required
        - (void)messageReceived:(NSString *)message;

    @optional
        - (void)connectionEstablished;
        - (void)connectionClosed;
        - (void)connectionKilled;
        - (void)errorOccurred:(NSError *)error;

@end


@interface GameClient : NSObject

    @property (weak, nonatomic) id<GameClientDelegate> delegate;

    - (id)initWithHost:(NSString *)host port:(int)port;
    - (void)connect;
    - (void)disconnect;
    - (void)sendMessage:(NSString *)message;
    - (void)sendStart:(char)player;
    - (void)sendMoveTo:(int)cell;
    - (void)sendQuit;

@end
