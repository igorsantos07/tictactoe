//
//  IGSMainViewController.m
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import "IGSMainViewController.h"
#import "IGSAppDelegate.h"
#import <objc/runtime.h>

NSUserDefaults * settings;
NSString * const KeyPlayer      = @"player";
NSString * const KeyCloudyIcon  = @"cloudyIcon";

int const PlayerClear           = 0;
int const PlayerCloudy          = 1;
int const CloudyOvercast        = 0;
int const CloudyShowers         = 1;
int const CloudyStorm           = 2;
int const CloudyThunderstorm    = 3;
int const CloudySnow            = 4;

int const StatWin       = 0;
int const StatTie       = 1;
int const StatLose      = 2;
int const StatGiveUp    = 3;
int const StatPlayed    = 4;
int const StatTotal     = 5;

//this pairs with PlayerClear and PlayerCloudy constants
int const PlayerTotals  = 2;

@interface IGSMainViewController ()
@end

@implementation IGSMainViewController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSettings];
}

- (void)setupSettings {
    settings = [NSUserDefaults standardUserDefaults];
    [settings registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithInt:1], KeyPlayer,
                                [NSNumber numberWithInt:0], KeyCloudyIcon,
                                nil]];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(settingsDidChange:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];
}

- (void)settingsDidChange:(NSNotification *)notification {
    [settings synchronize];
}

- (void)statAddToType:(int)type forPlayer:(int)player {
    NSLog(@"+1 to type %i for player %i", type, player);
    
    NSMutableArray * stat = [[self statistics] objectAtIndex:type];
    NSMutableArray * total = [[self statistics] objectAtIndex:StatTotal];
    
    //those lines ugly replace the needed NSNumber with an incremented version of they
    NSLog(@"%i", [self statForType:type player:player]);
    
    [self statIncrementByKey:player stat:stat];
    [self statIncrementByKey:player stat:total];
    [self statIncrementByKey:PlayerTotals stat:stat];
    [self statIncrementByKey:PlayerTotals stat:total];
    
    NSLog(@"%i", [self statForType:type player:player]);
}

- (void)statIncrementByKey:(int)key stat:(NSMutableArray *)stat {
    [stat replaceObjectAtIndex:key withObject:[NSNumber numberWithInt:([[stat objectAtIndex:key] intValue]+1)]];
}

- (int)statForType:(int)type player:(int)player {
    return [[[[self statistics] objectAtIndex:type] objectAtIndex:player] integerValue];
}
- (NSMutableArray *)statistics {
    IGSAppDelegate * delegate = (IGSAppDelegate *) [[UIApplication sharedApplication] delegate];
    return [delegate statistics];
}

@end
