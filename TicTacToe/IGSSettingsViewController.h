//
//  IGSSettingsViewController.h
//  TicTacToe
//
//  Created by  on 12-10-17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGSMainViewController.h"

@interface IGSSettingsViewController : IGSMainViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl *playerButton;
@property (strong, nonatomic) IBOutlet UIView *cloudyButtonsView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cloudyButtons;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *cloudyTexts;

- (IBAction)cloudyButtonPressed:(UIButton *)sender;
- (IBAction)playerChanged:(UISegmentedControl *)sender;

@end
