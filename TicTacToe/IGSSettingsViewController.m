//
//  IGSSettingsViewController.m
//  TicTacToe
//
//  Created by  on 12-10-17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IGSSettingsViewController.h"

@interface IGSSettingsViewController ()

@end

@implementation IGSSettingsViewController
@synthesize playerButton;
@synthesize cloudyButtonsView;
@synthesize cloudyButtons;
@synthesize cloudyTexts;

int CloudyButtonThunderstorm;
int CloudyButtonSnow;
int CloudyButtonOvercast;
int CloudyButtonShowers;
int CloudyButtonStorm;
int CloudyTextThunderstorm;
int CloudyTextSnow;
int CloudyTextOvercast;
int CloudyTextShowers;
int CloudyTextStorm;

float const notSelectedAlpha = 0.4;
float const selectedAlpha    = 1.0;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Settings", @"Settings");
        self.tabBarItem.image = [UIImage imageNamed:@"tab-settings"];
    }
    return self;
}
							
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTagNames];
    [self updateButtonsStyle]; //this should be plaved somewhere BEFORE the view load, as you can see quickly the buttons changing when you reopen the app from background
}

- (void)setTagNames {
    CloudyButtonThunderstorm    = CloudyThunderstorm;
    CloudyButtonSnow            = CloudySnow;
    CloudyButtonOvercast        = CloudyOvercast;
    CloudyButtonShowers         = CloudyShowers;
    CloudyButtonStorm           = CloudyStorm;
    CloudyTextThunderstorm      = 10 + CloudyThunderstorm;
    CloudyTextSnow              = 10 + CloudySnow;
    CloudyTextOvercast          = 10 + CloudyOvercast;
    CloudyTextShowers           = 10 + CloudyShowers;
    CloudyTextStorm             = 10 + CloudyStorm;
}

- (void)viewDidUnload {
    [self setCloudyButtonsView:nil];
    [self setCloudyButtons:nil];
    [self setCloudyTexts:nil];
    [self setCloudyTexts:nil];
    [self setPlayerButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)settingsDidChange:(NSNotification *)notification {
    [super settingsDidChange:notification];
    [settings synchronize];
    [self updateButtonsStyle];
}

- (void)updateButtonsStyle {
    NSLog(@"Updating buttons style!");
    
    [playerButton setSelectedSegmentIndex:[settings integerForKey:KeyPlayer]];
    
    int selectedButton = [settings integerForKey:KeyCloudyIcon],
        selectedText   = [settings integerForKey:KeyCloudyIcon] + 10;
    
    //setting the default alpha for all the other buttons that are not the right one - and setting its alpha to a higher value
    for (UIButton * button in cloudyButtons)
        [button setAlpha:(button.tag == selectedButton)? selectedAlpha : notSelectedAlpha];
    
    for (UILabel * label in cloudyTexts)
        [label setAlpha:(label.tag == selectedText)? selectedAlpha : notSelectedAlpha];
}

- (IBAction)cloudyButtonPressed:(UIButton *)sender {
    [settings setInteger:sender.tag forKey:KeyCloudyIcon];
}

- (IBAction)playerChanged:(UISegmentedControl *)sender {
    [settings setInteger:[sender selectedSegmentIndex] forKey:KeyPlayer];
}

@end
