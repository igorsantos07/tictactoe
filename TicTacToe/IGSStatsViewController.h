//
//  IGSStatsViewController.h
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGSMainViewController.h"

@interface IGSStatsViewController : IGSMainViewController
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *statsNumbers;

@end
