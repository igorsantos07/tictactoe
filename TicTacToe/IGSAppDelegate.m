//
//  IGSAppDelegate.m
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import "IGSAppDelegate.h"

#import "IGSGameViewController.h"
#import "IGSStatsViewController.h"
#import "IGSSettingsViewController.h"

@implementation IGSAppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;

NSMutableArray * statistics;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIViewController *viewControllerGame = [[IGSGameViewController alloc] initWithNibName:@"IGSGameViewController" bundle:nil];
    UIViewController *viewControllerStats = [[IGSStatsViewController alloc] initWithNibName:@"IGSStatsViewController" bundle:nil];
    UIViewController *viewControllerSettings = [[IGSSettingsViewController alloc] initWithNibName:@"IGSSettingsViewController" bundle:nil];
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:viewControllerGame, viewControllerStats, viewControllerSettings, nil];
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    
    [self readStatistics];
    
    return YES;
}

- (NSMutableArray *)statistics {
    return statistics;
}

- (void)readStatistics {
    NSString * file = [self dataFilePath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:file]) {
        NSError * error = nil;
        [@"" writeToFile:[self dataFilePath] atomically:true encoding:NSStringEncodingConversionAllowLossy error:&error];
    }
    statistics = [[NSMutableArray alloc] initWithContentsOfFile:file];
    statistics = [self setupStatistics];
}

- (NSMutableArray *)setupStatistics {
    NSMutableArray * emptySecondLevel = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
    
    //had to copy and return it back because for some reason using "statistics" directly wasn't work (wasn't adding objects)
    NSMutableArray * planB = [NSMutableArray arrayWithArray:[self statistics]];
    
    for (int type = StatWin; type <= StatTotal; type++) {
        if ([planB count] < (type+1)) {
            [planB addObject:[NSMutableArray arrayWithArray:emptySecondLevel]];
        }
    }
    
    return planB;
}

- (NSString *)dataFilePath {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, true);
    NSString * docs = [paths objectAtIndex:0];
    return [docs stringByAppendingPathComponent:@"stats.plist"];
}

- (void)writeStatistics {
    [[self statistics] writeToFile:[self dataFilePath] atomically:true];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [self writeStatistics];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
