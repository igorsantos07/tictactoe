//
//  IGSStatsViewController.m
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import "IGSStatsViewController.h"

@interface IGSStatsViewController ()

@end

@implementation IGSStatsViewController
@synthesize statsNumbers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Statistics", @"Statistics");
        self.tabBarItem.image = [UIImage imageNamed:@"tab-statistics-bw"];
    }
    return self;
}
							
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    int player, stat;
    for (UILabel * label in statsNumbers) {
        NSString * number = [NSString stringWithFormat:@"%i", label.tag];
        if (label.tag > 10) {
            player = [[NSString stringWithFormat:@"%c", [number characterAtIndex:0]] integerValue];
            stat   = [[NSString stringWithFormat:@"%c", [number characterAtIndex:1]] integerValue];
        }
        else {
            player = 0;
            stat   = [[NSString stringWithFormat:@"%c", [number characterAtIndex:0]] integerValue];
        }
        
        int value = [self statForType:stat player:player];
        label.text = [NSString stringWithFormat:@"%i", value];
    }
}

- (void)viewDidUnload {
    [self setStatsNumbers:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

@end
