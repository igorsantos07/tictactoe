//
//  IGSGameViewController.h
//  TicTacToe
//
//  Created by Igor Santos.
//  Copyleft (c) 2012 igorsantos07. Some rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGSMainViewController.h"
#import "GameClient.h"

@interface IGSGameViewController : IGSMainViewController <GameClientDelegate>
    @property (strong, nonatomic) GameClient * client;

    /****** Connection screen ******/
    @property (strong, nonatomic) IBOutlet UIView *viewConnection;
    @property (strong, nonatomic) IBOutlet UITextField *fieldServerIP;
    @property (strong, nonatomic) IBOutlet UITextField *fieldServerPort;
    @property (strong, nonatomic) IBOutlet UIView *alertError;
    @property (strong, nonatomic) IBOutlet UILabel *errorLabel;
    @property (strong, nonatomic) IBOutlet UISegmentedControl *errorSegmentedButton;
    - (IBAction)userDidStartGame;
    - (IBAction)buttonRetryPressed:(UISegmentedControl *)sender;
    - (void)attemptReconnection;

    /******* Gameboard screen ******/
    @property (strong, nonatomic) IBOutlet UIView *viewGame;
    @property (strong, nonatomic) IBOutlet UIView *indicatorBox;
    @property (strong, nonatomic) IBOutlet UIImageView *indicatorImg;
    @property (strong, nonatomic) IBOutlet UILabel *indicatorTxt;
    @property (strong, nonatomic) IBOutlet UIView *alertGameOver;
    @property (strong, nonatomic) IBOutlet UIImageView *gameOverFace;
    @property (strong, nonatomic) IBOutlet UILabel *gameOverLabel;
    @property (strong, nonatomic) IBOutlet UISegmentedControl *gameOverSegmentedButton;
    @property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *boardButtons;
    - (IBAction)gameOverButtonsPressed:(UISegmentedControl *)sender;
    - (IBAction)playerMoved:(id)sender;
    - (IBAction)userDidGiveUp:(id)sender;
    - (void)connectionEstablished;
@end
